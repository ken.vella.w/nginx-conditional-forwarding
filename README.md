# nginx-conditional-forwarding

This is a demo application showing minimal configuration needed within NginX to create a reverse proxy that condtionally forwards to an application based on the existance of a HTTP header.

## Conditional Forwarding

The loadbalancer evaluates a HTTP Header that is configured to be `wtae-checkout-123`. If this header **exists** traffic will be forwarded from the loadbalancer to the `mock_server`. If this header does **not** exist then forward to the `app_server`.

## Entities

* client - external app or user accessing the service
* [loadbalancer](loadbalancer/) - the NginX proxy that does conditional forwarding to either the `app_sever` or `mock_server`
* [app_server](app_server/) - an application serving data, configuration found within
* [mock_server](mock_server/) - a mocked server serving data

## Diagram

```mermaid
flowchart TB
    client
    loadbalancer
    app_server
    mock_server
    
    client --> loadbalancer
    loadbalancer --> app_server
    loadbalancer -- -H 'wtae-checkout-123: true'--> mock_server
```

## Usage
This demo was built within a SELinux environment using `podman` and `podman-compose`. To run this without SELinux do not use the `:Z` suffix on volume definitions within [docker-compose.yaml](docker-compose.yaml). `podman` and `podman-compose` should be interchangeable with `docker` and `docker-compose`.

A [Makefile](Makefile) is included with this project to help with running the containers using `podman-compose` and also testing the loadbalancer using `curl`. To list all the makefile options just type `make` within the root directory.
