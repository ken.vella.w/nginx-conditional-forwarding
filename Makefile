.ONESHELL :
.PHONY = help
.DEFAULT_GOAL := help

run: ## setup and run the docker containers
	podman-compose up -d

clean: ## clean up docker containers
	podman-compose down

test_mock: ## access the mock
	curl -H "wtae-checkout-123: foo" http://localhost:8080/hello.html

test_app: ## access the app
	curl http://localhost:8080/hello.html

help:
	@grep -E '^[/a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
